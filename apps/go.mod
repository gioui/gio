module gioui.org/apps

go 1.13

require (
	gioui.org/ui v0.0.0-20190902131729-7ad60e08787b
	github.com/google/go-github/v24 v24.0.1
	golang.org/x/exp v0.0.0-20190627132806-fd42eb6b336f
	golang.org/x/image v0.0.0-20190703141733-d6a02ce849c9
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45
)
